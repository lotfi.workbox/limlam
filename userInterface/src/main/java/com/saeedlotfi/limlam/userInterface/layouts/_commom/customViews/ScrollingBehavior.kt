package com.saeedlotfi.limlam.userInterface.layouts._commom.customViews

import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.appbar.AppBarLayout

class ScrollingBehavior : AppBarLayout.ScrollingViewBehavior() {

    private var toolbarHeight = 0
    private var flag = false
    fun getStaticToolbarHeight(height : Int){
        this.toolbarHeight = height
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: View, dependency: View): Boolean {
        if (dependency !is AppBarLayout) {
            return super.onDependentViewChanged(parent, child, dependency)
        }
        if (!flag){
            (parent.layoutParams as CoordinatorLayout.LayoutParams).height = parent.height + toolbarHeight
            flag = true
        }
        return super.onDependentViewChanged(parent, child, dependency)
    }
}